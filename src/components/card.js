import React from "react";
import "./card.css";

function Card(props) {
  return (
    <div className="day-card">
      <img src={props.image_url} alt="pic" className="location-img" />
      <div className="day-details">
        <div className="location-data">
          <img src="./location.png" alt="map" className="map-logo" />
          <p>{props.location}</p>
          <a href={props.googlemapsurl} target="_blank" rel="noreferrer">
            View location on google maps
          </a>
        </div>
        <div className="details">
          <h3>{props.title}</h3>
          <p>{props.visit_date}</p>
          <p>{props.description}</p>
        </div>
      </div>
    </div>
  );
}

export { Card };
