const data =[
    {
        title : "Port Melbourne Beach",
        location:"Melbourne",
        visit_date:"05-12-2021",
        googlemapsurl:"https://goo.gl/maps/G5mDziuPExosK7Vb7",
        image_url:"https://event-hubs.s3.amazonaws.com/WhatsOnStKilda/2022/port-melbourne.jpg",
        description:"The first place I visited after coming to Melbourne."
    },
    {
        title : "St Kilda Beach",
        location:"Melbourne",
        visit_date:"05-12-2021",
        googlemapsurl:"https://goo.gl/maps/r9yCxw4dNoCHvj9k6",
        image_url:"https://upload.wikimedia.org/wikipedia/commons/0/03/St_Kilda_Pier_and_Kiosk_with_Catani_Gardens_in_foreground_-_panoramio.jpg",
        description:"It is a very nice Beach."
    },
    {
        title : "Royal Botanical Garden",
        location:"Melbourne",
        visit_date:"12-12-2021",
        googlemapsurl:"https://goo.gl/maps/Qt2691LGNSoRLRnLA",
        image_url:"https://whatson.melbourne.vic.gov.au/rails/active_storage/representations/proxy/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt3TlRka09UQTROQzA0TkdSa0xUUXlOREV0WVdNMU9TMDBaREEyTnpJNU9EazFNR0lHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6ImJsb2JfaWQifX0=--c9faaeb033a7e1081e8779bb9befd2f66ba702fa/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJYW5CbkJqb0dSVlE2RkhKbGMybDZaVjkwYjE5c2FXMXBkRnNIYVFMb0Eya0NXQUk9IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--5c7b32c6a74ee88f2f4469ce3a4ae709d1994efe/9022d12d-56c4-441c-ba61-e2a44c09fb76.jpg",
        description:"We can see a lot of different plants. It is a good place to visit."
    },
    {
        title : "Eureka Skydeck",
        location:"Melbourne",
        visit_date:"05-12-2021",
        googlemapsurl:"https://goo.gl/maps/Go2WWaxQrPqYb8Wc9",
        image_url:"https://www.visitvictoria.com/-/media/atdw/melbourne/things-to-do/art-theatre-and-culture/architecture-and-design/90d83c6e13f601e72f7e7d6f8921e2a9_1600x1200.jpeg?ts=20210707300229",
        description:"The edge experience is very great. we can observe the beauty of melbourne from level 88."
    },
    {
        title : "Brighton Beach",
        location:"Brighton",
        visit_date:"01-01-2022",
        googlemapsurl:"https://goo.gl/maps/j7DJ3UDumVAadPMB7",
        image_url:"https://cdn.theculturetrip.com/wp-content/uploads/2020/09/2ab312f-e1599838041510.jpg",
        description:"It is situated at brighton like 30min jouney from Melbourne CBD."
    },
    {
        title : "Williams Town Beach",
        location:"Williams Town",
        visit_date:"10-07-2022",
        googlemapsurl:"https://goo.gl/maps/AC9r8ABZJ5GbRJcU9",
        image_url:"https://www.worldbeachguide.com/photos/williamstown-beach.jpg",
        description:"It is 45min Away from Melboune CBD."
    }
]

export default data