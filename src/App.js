import data from "./components/data";
import Travel from "./components/travel";
import { Card } from "./components/card";

import "./App.css";

function App() {
  return (
    <div>
      <Travel></Travel>
      {data.map((trip) => {
        return <Card {...trip} />;
      })}
    </div>
  );
}

export default App;
